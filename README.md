# UEF Elearn Dark Style
This repository contains alternative css for [elearn.uef.fi](https://elearn.uef.fi).
![image](https://cdn.discordapp.com/attachments/492188265487532032/1039699212117491732/image.png)


## Getting started
- Install addon called [stylus](https://add0n.com/stylus.html) to modify website styles.
- Install style from [stylus](https://userstyles.world/style/7235/dark-mode-for-uef-elearn).

## Development
- Install addon called [stylus](https://add0n.com/stylus.html) to modify website styles.
- After installing go to [elearn](https://elearn.uef.fi) and press addon.
- Select `write style for: this URL`.
- Add content from [style.css](style.css).

## Contributing
Feel free to make your own changes to the code.
If you find bugs or other thing to improve the style and want to add changes here, you can make pull request to this repository.
main branch will be synced with [stylus](https://userstyles.world/style/7235/dark-mode-for-uef-elearn).
